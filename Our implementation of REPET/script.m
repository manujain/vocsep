function y = script(x,fs,per)

if nargin < 3, per = [0.8,min(8,(length(x)/fs)/3)]; end

len = 0.040;
N = 2.^nextpow2(len*fs);
win = hamming(N,'periodic');
stp = N/2;

cof = 100;
cof = ceil(cof*(N-1)/fs);

[t,k] = size(x);
X = [];
for i = 1:k     
    Xi = stft(x(:,i),win,stp);
    X = cat(3,X,Xi);
end
plot(X)
V = abs(X(1:N/2+1,:,:));

per = ceil((per*fs+N/stp-1)/stp);
if numel(per) == 1
    p = per;
elseif numel(per) == 2
    b = BeatSpectrum(mean(V.^2,3));
    figure, plot(b)
    p = RepeatingPeriodFunc(b,per);
end

y = zeros(t,k);
for i = 1:k 
    Mi = RepeatingMaskFunc(V(:,:,i),p);
    Mi(1+(1:cof),:) = 1;
    Mi = cat(1,Mi,flipud(Mi(2:end-1,:)));
    yi = istft(Mi.*X(:,:,i),win,stp);
    y(:,i) = yi(1:t);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   Short-Time Fourier Transform (STFT) using fft
%       X = stft(x,win,stp);
%
%   Input(s):
%       x: signal [t samples, 1]
%       win: analysis window [N samples, 1]
%       stp: analysis step
%
%   Output(s):
%       X: Short-Time Fourier Transform [N bins, m frames]

function StftMatrix  = stft(Sample, Window, StepSize)
SampleLength = length(Sample);
WindowSize = length(Window);
LeftPadding = WindowSize - StepSize;
TotalSegments = floor((WindowSize - StepSize + SampleLength)/StepSize);
RightPadding = (TotalSegments + 1)*StepSize - SampleLength; 
Sample = [zeros(LeftPadding, 1); Sample; zeros(RightPadding, 1)];
StftMatrix = zeros(WindowSize, TotalSegments + 1);
for i = 1:TotalSegments + 1
    StftMatrix(:,i) = fft(Sample((1:WindowSize) + StepSize*(i-1)).* Window);
end

%function X = stft(x,win,stp)
%t = length(x);                                                              % Number of samples
%N = length(win);                                                            % Analysis window length
%m = ceil((N-stp+t)/stp);                                                    % Number of frames with zero-padding
%x = [zeros(N-stp,1);x;zeros(m*stp-t,1)];                                    % Zero-padding for constant overlap-add
%X = zeros(N,m);
%for j = 1:m                                                                 % Loop over the frames
 %   X(:,j) = fft(x((1:N)+stp*(j-1)).*win);                                  % Windowing and fft
%end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   Inverse Short-Time Fourier Transform using ifft
%       x = istft(X,win,stp);
%
%   Input(s):
%       X: Short-Time Fourier Transform [N bins, m frames]
%       win: analysis window [N samples, 1]
%       stp: analysis step
%
%   Output(s):
%       x: signal [t samples, 1]

function Signal = istft(StftMatrx, Window, StepSize)
[Rows, Columns] = size(StftMatrx);
SampleLength = (Columns - 1) * StepSize + Rows;
Signal = zeros(SampleLength,1);
for i = 1:Columns
    Signal((1:Rows) + StepSize*(i-1)) = Signal((1:Rows) + StepSize*(i-1)) + real(ifft(StftMatrx(:,i)));
end
Signal(SampleLength - (Rows - StepSize) + 1:SampleLength) = [];
Signal(1:Rows - StepSize) = [];
Signal = Signal / sum(Window(1:StepSize:Rows));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   Autocorrelation function using fft according to the Wiener�Khinchin theorem
%       C = acorr(X);
%
%   Input(s):
%       X: data matrix [n elements, m vectors]
%
%   Output(s):
%       C: autocorrelation matrix [n lags, m vectors]

function AutoCorrelationMatrx = AutoCorrelate(Sample)
[Rows, Columns] = size(Sample);
Sample = [Sample; zeros(Rows, Columns)];
MagnitudeSpectrogram = abs(fft(Sample));
AutoCorrelationMatrx = MagnitudeSpectrogram.^2;
AutoCorrelationMatrx = ifft(AutoCorrelationMatrx);
AutoCorrelationMatrx = AutoCorrelationMatrx(1:Rows,:);
AutoCorrelationMatrx = AutoCorrelationMatrx./repmat((Rows:-1:1)',[1,Columns]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   Beat spectrum using the autocorrelation function
%       b = beat_spectrum(X);
%
%   Input(s):
%       X: spectrogram [n frequency bins, m time frames]
%
%   Output(s):
%       b: beat spectrum [1, m time lags]

function b = BeatSpectrum(Sample)
AutoCorrelationMatrix = AutoCorrelate(Sample');
b = mean(AutoCorrelationMatrix, 2);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   Repeating period from the beat spectrum
%       p = repeating_period(b,r);
%
%   Input(s):
%       b: beat spectrum [1, m time lags]
%       r: repeating period range in time frames [min lag, max lag]
%
%   Output(s):
%       p: repeating period in time frames

function RepeatingPeriod = RepeatingPeriodFunc(BeatSpectrumMatrix,RepeatingPeriodRange)
BeatSpectrumMatrix(1) = [];
BeatSpectrumMatrix = BeatSpectrumMatrix(RepeatingPeriodRange(1):RepeatingPeriodRange(2));
[~,RepeatingPeriod] = max(BeatSpectrumMatrix);
RepeatingPeriod = RepeatingPeriod+RepeatingPeriodRange(1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%   Repeating mask from the magnitude spectrogram and the repeating period
%       M = repeating_mask(V,p);
%
%   Input(s):
%       V: magnitude spectrogram [n bins, m frames]
%       p: repeating period in time frames
%
%   Output(s):
%       M: repeating mask in [0,1] [n bins, m frames]

function RepeatingMask = RepeatingMaskFunc(MagnitudeSpectrogram,RepeatingPeriod)
[FrequencyBins,TimeFrames] = size(MagnitudeSpectrogram);
RepeatingSegments = ceil(TimeFrames/RepeatingPeriod);
Temp = [MagnitudeSpectrogram,nan(FrequencyBins,RepeatingSegments*RepeatingPeriod-TimeFrames)];
Temp = reshape(Temp,[FrequencyBins*RepeatingPeriod,RepeatingSegments]);
Temp = [median(Temp(1:FrequencyBins*(TimeFrames-(RepeatingSegments-1)*RepeatingPeriod),1:RepeatingSegments),2); ...
    median(Temp(FrequencyBins*(TimeFrames-(RepeatingSegments-1)*RepeatingPeriod)+1:FrequencyBins*RepeatingPeriod,1:RepeatingSegments-1),2)];
Temp = reshape(repmat(Temp,[1,RepeatingSegments]),[FrequencyBins,RepeatingSegments*RepeatingPeriod]);
Temp = Temp(:,1:TimeFrames);
Temp = min(MagnitudeSpectrogram,Temp);
RepeatingMask = (Temp+eps)./(MagnitudeSpectrogram+eps);